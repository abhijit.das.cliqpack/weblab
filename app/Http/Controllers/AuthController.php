<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Contructs\UserContruct;

class AuthController extends Controller
{
    private $userRepository;

    public function __construct(UserContruct $userRepository)
    {
        $this->userRepository=$userRepository;
    }

    public function register(Request $request){
        $userStore=$this->userRepository->store($request->all());
        return $userStore;
    }
    public function login(Request $request){
        $userGet=$this->userRepository->findUser($request->all());
        return $userGet;
    }
    public function userInfo(){
        $userInfo=$this->userRepository->findUserInfo();
        return $userInfo;
    }
}

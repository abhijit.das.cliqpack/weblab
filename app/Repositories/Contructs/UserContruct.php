<?php
 namespace App\Repositories\Contructs;

 interface UserContruct {
     
    public function store(array $user);
    public function findUserInfo();
    public function findUser(array $user);
    public function findVerified();
 }


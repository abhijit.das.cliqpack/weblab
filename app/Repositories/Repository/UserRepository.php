<?php

namespace App\Repositories\Repository;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Contructs\UserContruct;
use App\Models\User;

//use DataTables;

class UserRepository implements UserContruct
{

    public function store(array $user)
    {
        try {
            $validator = Validator::make($user, [
                'first_name' => 'required|min:2|max:255', 
                'last_name' => 'required|min:2|max:255', 
                'email'=> 'required|email|max:255', 
                'user_type'=> 'required|min:2|max:255', 
                'password' => 'required|min:8'
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'message' => 'Validation Error',
                    'data' => null,
                    'error' => $validator->errors()
                ], 422);
            } else {
                $user = User::create([
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'email' => $user['email'],
                    'user_type' => $user['user_type'],
                    'work_phone' => $user['work_phone'],
                    'mobile_phone' => $user['mobile_phone'],
                    'password' =>  Hash::make($user['password'])
                ]);

                $userToken = $user->createToken('Token')->accessToken;

                return response()->json([
                    'token' => $userToken,
                    'data' => ['user' => $user],
                    'message' => 'Registration Successfull'
                ], 200);
            }
            
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Opps! An Exception',
                'data' => null,
                'error' => $e->getMessage()
            ], 410);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Opps! A Query Exception',
                'data' => null,
                'error' => $e->getMessage()
            ], 411);
        }
    }

    public function findUserInfo()
    {
        try {
            $user = auth()->user();
            return response()->json([
                'message' => 'Login Successfull',
                'data' => $user,
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Opps! An Exception',
                'data' => null,
                'error' => $e->getMessage()
            ], 410);
        }
    }

    public function findUser(array $user)
    {
        try {
            $validator = Validator::make($user, [
                'email' => 'required|email',
                'password' => 'required|min:8'
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'token' => null,
                    'message' => 'Login Error',
                    'error' => $validator->errors()
                ], 422);
            } else {
                $data = [
                    'email' => $user['email'],
                    'password' => $user['password']
                ];

                if (auth()->attempt($data)) {
                    $userToken = auth()->user()->createToken('Token')->accessToken;

                    return response()->json([
                        'token' => $userToken,
                        'message' => 'Login Successfull'
                    ], 200);
                } else {
                    return response()->json([
                        'token' => null,
                        'message' => 'Unauthorized',
                        'error' => 'Unauthorized'
                    ], 410);
                }
            }
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Opps! An Exception',
                'data' => null,
                'error' => $e->getMessage()
            ], 410);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Opps! A Query Exception',
                'data' => null,
                'error' => $e->getMessage()
            ], 411);
        }
    }
}
